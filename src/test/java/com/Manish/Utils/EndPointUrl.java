package com.Manish.Utils;

public enum EndPointUrl {

	GET_SINGLE_USER("/api/users/"),
	DELETE_CUSTOMER("/Customer/Delete"),
	GET_LIST_OF_USER_PAGE_WISE("/api/users?page="),
	POST_USER("/api/users"),
	POST_LOGIN("/api/login");
	
	String uri;
	
	EndPointUrl(String uri){
		this.uri=uri;
	}
	
	public String getUri(){
		return this.uri;
	}
	
	public String getUri(String data){
		return this.uri+data;
	}
	
	/*public static void main(String[] s){
		System.out.println(EndPointUrl.GET_USER_BY_ID.getUri("2"));
		System.out.println(Urls.fixUrl+EndPointUrl.GET_USER_BY_ID.getUri("2"));
	}*/
}


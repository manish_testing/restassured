package com.Manish.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ConfigProp {

	static Properties prop;
	
	public static Properties getPropertiesFileData() throws IOException{
		String userDir=System.getProperty("user.dir");
		System.out.println(userDir);
		//File file = new File("C:\\Users\\ranjana\\workspace\\WebService-Project\\resources\\Configuration.properties");
		File file = new File(".\\resources\\Configuration.properties");
		FileInputStream fis= new FileInputStream(file);
		prop= new Properties();
		prop.load(fis);
		return prop;
	}
	
	public static Workbook getWorkBook() throws IOException{
		File file= new File(""+ConfigProp.getPropertiesFileData().getProperty("fil2Path")+"");
		FileInputStream fis= new FileInputStream(file);
		Workbook wb=new XSSFWorkbook(fis);
		return wb;
		
		
	}
	
	
}

package com.Manish.webservice.methods;

import java.io.IOException;
import java.util.Properties;

import org.json.JSONException;

import com.Manish.Utils.ConfigProp;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class WebServices {
	
	Properties pro;
	public static String authToken;
	
	public static Response Post(String uRI, String json){
		RequestSpecification requestSpecification=RestAssured.given().body(json);
		requestSpecification.contentType(ContentType.JSON);
		Response response=requestSpecification.post(uRI);
		return response;
		//For posting the data in the request for auth validation we need to use RequestSpecification
		//reference and call auth() method and on that auth method we can call basic authentication
		//by passing "uName" and "pswd" or we can call oauth method where we put four parameters
		//clicnt_secret,scope,grant_Type,client_id and in return of that we use to get token which was 
		//having the type as bearer and  value hexadecimal values and we send those token in login
		// header by generating a map object
		/*Response response=requestSpecification.auth().basic(userName, password);
		Response response=requestSpecification.auth().oauth(consumerKey, consumerSecret, accessToken, secretToken);
		 */		
	
		
	}
	
	public static Response Get(String uRI){
		RequestSpecification req=RestAssured.given();
		req.contentType(ContentType.JSON);
		Response response=req.get(uRI);
		return response;
	}
	
	public static Response Put(String uRI,String Json){
		RequestSpecification req=RestAssured.given().body(Json);
		req.contentType(ContentType.JSON);
		Response response=req.put(uRI);
		return response;
		
	}
	
	public static Response Delete(String uRI){
		RequestSpecification req=RestAssured.given();
		req.contentType(ContentType.JSON);
		Response response=req.delete(uRI);
		return response;
	}
	
	/*public static void PostCallWithHeader(String uRI, String Json) throws IOException{
		RequestSpecification req=RestAssured.given().body(Json);
		String userName=ConfigProp.getPropertiesFileData().getProperty("email");
		String password=ConfigProp.getPropertiesFileData().getProperty("passWord");
		
		//req.authentication().basic(userName, password)
	}*/
	
	public static String loginToApplicationandsetAuthToken(String uRI,String json) throws IOException, JSONException{
		
		//this below commented line would be used when u have username and password but the site
		 /*having email instead of username and passord hence i am not using the below commented form to
		 post username and password*/
		/*RequestSpecification req=RestAssured.given().auth().form(ConfigProp.getPropertiesFileData().getProperty("email"),ConfigProp.getPropertiesFileData().getProperty("passWord"));
		Response response=req.get(uRI);*/
		RequestSpecification req =RestAssured.given().body(json);
		req.contentType(ContentType.JSON);
		Response res=req.post(uRI);
		System.out.println(res.asString());
		if(res.statusCode()==200){
			org.json.JSONObject jsonobj= new org.json.JSONObject(res.asString());
			authToken=jsonobj.getString("token");
			System.out.println(authToken);
			
		}
		else{
			authToken=null;
		}
		return authToken;
	}
	
	
	
	
	

}
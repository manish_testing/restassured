package com.Manish.testScripts;

import org.testng.annotations.Test;

import junit.framework.Assert;

public class VerifyingFailedTestcaseWithRetryAnalyser {

	//first method to add retryAnalyser parameter on per Testlevel
	@Test(retryAnalyzer=com.Manish.testScripts.RetryAnalyser.class)
	public void m1(){
		String str="Manish";
		Assert.assertEquals(str, "Manis");
		
	}
	
	
	/*Second method to add retry annotation at runtime
	 * here we have not added retryanalyserParameter but in testng.xml we have added RetryAnalyserAtRunTime
	class which is custom class(own class) where in we have implemented IAnnotationTransformer interface through 
	which we call transform method and in that transform method we have argumnet which is 
	 ITestannotation interface which is a kind of Listener and this listener we give in TestNG.xml
	 and with reference name annotation we call method  setretryanalyser 
	 and in the argument of setRetryAnalyzer we pass owr own retryclassanalyser.class which implements 
	 IRetryAnalyzer interface*/
	@Test
	public void m2(){
		String str=new String("manis");
		String str1=new String("manis");
		System.out.println(str.hashCode()); // here the hashcode is same
		System.out.println(str1.hashCode());
		//Assert.assertEquals(str, str1); // it will match the content of string hence will pass
		Assert.assertSame(str, str1);// it is comparing both the object which is not equal
		/*if(str==str1){
		
		System.out.println("assertion passed");
		}*/
	}
	
}

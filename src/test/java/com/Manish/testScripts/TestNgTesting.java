package com.Manish.testScripts;

import java.util.ArrayList;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import junit.framework.Assert;

/*This is also the way to imlement the listener on particular class by givin the listener class full
qualified name.class, We can also give on the suite level on xml file giving the tag as <listeners>
and under listeners tag give the tag as <listener class-name="fully qualified name of the class">*/ 

//@Listeners(com.Manish.testScripts.Listener.class) 
public class TestNgTesting {

	/*@BeforeTest
	public void beforTestInTestNgTestingClass(){
		System.out.println("TestNgTestingClass :Before Test annotation");
	}
	
	
	@BeforeGroups("smokeTest")
	public void beforGroupsTest(){
		System.out.println("BeforeGroups");
	}
	
	
	@AfterGroups("smokeTest")
	public void afterGroupsTest(){
		System.out.println("AfterGroups");
		
	}
	
	@AfterTest
	public void AfterTest(){
		System.out.println("AfterTest inTestNgTestingclass");
	}
	
	@AfterSuite
	public void afterSuiteInTestNgTestingClass(){
		System.out.println("In TestNgAfterSuiteclassmethod : afterSuiteInTestNgTestingClass");
	}
	*/
	
	/*@Test(groups={"smokeTest"},enabled=true)
	public void testMethod(){
		System.out.println("In TestNgTestingclassmethod : first test case for group somke test");
	}
	
	@Test(description="This is second test case",groups={"regressionTest","smokeTest"})
	public void testMethod2(){
		System.out.println("In TestNgTestingclassmethod : Second test case for regression and smoke test");
	}
	
	
	//@Test(dependsOnMethods="verifycallingmethod") can be written like this alsi if depends on one method
	@Test(dependsOnMethods={"runBeforeVerifyCallingMethod","verifycallingmethod"})
	public void checkDependsOn(){
		System.out.println("dependent method");
	}
	
	@Test
	public void verifycallingmethod(){
		System.out.println("running before checDependsOn");
	}
	
	@Test
	public void runBeforeVerifyCallingMethod(){
		System.out.println("should run runVerifyCallingMethod");
	}
	@Test(expectedExceptions={NumberFormatException.class,Exception.class})
	public void checkExceptionm1(){
		System.out.println("in exception class");
		String i="110.23";
		System.out.println(Integer.parseInt(i));
	}*/
	
	@Test(invocationCount=1, successPercentage=100)
	@Parameters("myName")
	public void checkingForTestLevelParameters(boolean str){
		boolean i=true;
		Assert.assertEquals(i, str);
		System.out.println("assertion passed");
	}
	
	/*@Test
	@Parameters("mName")
	public void verifyParametrizedTest(String Name){
		String str= "Hello "+Name+"";
		System.out.println(str);
	}*/
	
	
}

package com.Manish.testScripts;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestNgLearningPurpose {

	
	@BeforeSuite
	public void beforSuite(){
		System.out.println("Before Suit method");
	}
	@AfterSuite
	public void afterSuit(){
		System.out.println("After Suit method");
	}
	@BeforeTest
	public void beforeTest(){
		System.out.println("Before Test");
	}
	@BeforeMethod
	public void beforMethos(){
		System.out.println("before method");
	}
	@AfterMethod
	public void AfterMethod(){
		System.out.println("After method");
	}
	
	
	@Test(groups={"smokeTest"})
	public void test1(){
		System.out.println("in test1 under smoke method");
	}
	
	@Test(groups={"regressionTest"})
	public void test2(){
		System.out.println("in test2 under regressiotetest  method");
	}
	
	@Test(groups={"smokeTest","regressionTest"})
	public void test3(){
		System.out.println("in test3 method under regression&smoke test case");
	}
	@AfterTest
	public void AfterTest(){
		System.out.println("After Test");
	}
	@BeforeGroups
	public void beforeGroups(){
		System.out.println("Before Groups");
	}
	@AfterGroups
	public void afterGroups(){
		System.out.println("After groups");
	}
	@BeforeClass
	public void beforeClass(){
		System.out.println("Before Class");
	}
	@AfterClass
	public void afterClass(){
		System.out.println("After Class");
	}
	/*@BeforeMethod
	public void beforeMethod(){
		System.out.println("before Method");
	}*/
	/*@AfterMethod
	public void afterMethod(){
		System.out.println("after method");
	}*/
}

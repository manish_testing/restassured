package com.Manish.testScripts;

import java.util.Iterator;
import java.util.Set;

import org.testng.IResultMap;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;

public class Listener implements ITestListener {

	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println("OnTestStart Test Case name : "+result.getMethod()+"-----");
		System.out.println(result.getName());
	}

	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		
		System.out.println("If passed the testcase name on Test Suucess "+result.getMethod()+"****");
		
	}

	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println("If Failed the testcase name on Test Failure "+result.getMethod()+"&&&&&&");
	}

	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println("If Skipped the testcase name "+result.getMethod()+"");
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println("withinsuccesspercentage ::::: "+result.getName());
		
	}

	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		System.out.println("in OnStart method : "+context.getName()+"");
	}

	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		System.out.println("in on finishmethod");
		IResultMap map=context.getPassedTests();
		System.out.println("Passed Test cases are ");
		for(ITestResult map1:map.getAllResults()){
			System.out.print(map1.getName()+"***************"+"\n");
		}
		IResultMap map1=context.getFailedTests();
		for(ITestResult map2:map1.getAllResults()){
			System.out.print("Failed test Cases are  "+map2.getName()+"***************" +"\n");
			
		}
		/*Set<ITestResult> set=map.getAllResults();
		Iterator<ITestResult> result=set.iterator();
		if(result.hasNext()){
			result.next().;
		}
	*/	System.out.println("in On Finish method : "+context.getName()+"");
	}

}

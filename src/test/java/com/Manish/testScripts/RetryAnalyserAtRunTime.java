package com.Manish.testScripts;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;

public class RetryAnalyserAtRunTime implements IAnnotationTransformer {
	
	public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor,
			Method testMethod){
		
		annotation.setRetryAnalyzer(com.Manish.testScripts.RetryAnalyser.class);
	}
	


}

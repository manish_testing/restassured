package com.Manish.testScripts;

import java.io.IOException;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.Manish.Pojo.Datum;
import com.Manish.Pojo.Login;
import com.Manish.Pojo.UserAdd;
import com.Manish.Pojo.UserList;
import com.Manish.Utils.ConfigProp;
import com.Manish.Utils.EndPointUrl;
import com.Manish.Utils.Urls;
import com.Manish.webservice.methods.WebServices;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class TestCaseRunner {
	Workbook wb;
Response response;
Gson gson= new GsonBuilder().create();
//for Gson knowledege take the help from below site
//http://tutorials.jenkov.com/java-json/gson.html

UserList user;
ArrayList<Integer> id=new ArrayList<Integer>();
ArrayList<String> lastName= new ArrayList<String>();
ArrayList<String> first_name= new ArrayList<String>();
ArrayList<String> email=new ArrayList<String>();
ArrayList<String> url=new ArrayList<String>();

	int count2 = 0;

	@BeforeTest
	public void test(){
		System.out.println("BeforeTest in testrunnerclass");
	}
	
	
	@BeforeClass
	public ArrayList<String> preRequisite() {
		ArrayList<String> al = new ArrayList<String>();
		al.add("George");
		al.add("Janet");
		al.add("Emma");
		al.add("Eve");
		al.add("Charles");
		al.add("Tracey");
		return al;

	}
	
	@Test(priority=1, dependsOnGroups="smoke", enabled=true)
	public void verifySingleUserData_Tc_001() throws ParseException{
		
		String uRI=(Urls.fixUrl+EndPointUrl.GET_SINGLE_USER.getUri("1"));
		response=WebServices.Get(uRI);
		
		
		//this xml path is same as jsonpath to parse the response in xml 
				/*XmlPath xmlpath=response.xmlPath();
				xmlpath.get("data.id");*/
		
		/*1) first Way to get the response as JsonPath, through which we can
		traverse with the root node by its get method
		 response.jsonpath treated same as jsonobject , both the class return jsonobject
		we can reach out to any node by traversing with the parent node
		here in my case the parent node is data having value as object, hence data.id, data.email
		we can reach by this if data was array then we could have reached by data(0).id*/ 
		int id=response.jsonPath().get("data.id");
		JsonPath jsonp=response.jsonPath();
		String email=jsonp.get("data.email");
		Assert.assertEquals(1,id);
		Assert.assertEquals(email, "george.bluth@reqres.in", "matching");
		System.out.println(response.getBody().asString()); //returns ResponseBody and converting that to String
		
		/*2) JsonParser (this JSONParser belowngs from com.google.gson.JsonParser which
		is also having parse method ans accept json string
		and which returns jsonElement then we call getAsJsonObject() 
		which converts jsonstring in to JsonObject)*/
		com.google.gson.JsonParser jsonp1= new JsonParser();
		JsonObject jsonp2=(JsonObject)jsonp1.parse(response.getBody().asString()).getAsJsonObject();// here response.asString comes stringyfy the json object
		System.out.println(jsonp2.getClass());// just checking the type of object
		String email1=jsonp2.get("data").getAsJsonObject().get("email").toString();// here we are getting the response
		//where data having the value as an object and on that data key we are calling method as getAsJsonBject
		//and then we are calling the elements through which we can get the element value 
		System.out.println(email1);
		Assert.assertEquals(email, "george.bluth@reqres.in", "matching");
		
		/*3) Now with the below simple.parser
		JSONParser //(This is exported from org.json.simple.parser, this having parse method which asscept st
		string as an argument and returns JSONObject)*/
		org.json.simple.parser.JSONParser jsonp3= new JSONParser();
		JSONObject jsonobj=(JSONObject)jsonp3.parse(response.getBody().asString());// converting response
		//to string and with the parse method of JSONParser converting to JSONObject
		JSONObject data= (JSONObject)jsonobj.get("data");// getting the first object data and then 
		//traversing to its field for getting field value
		String firstName=(String)data.get("first_name");// casting to string as the return type is Object
		Assert.assertEquals(firstName, "George");// assertion for checking the value is matched or not
		
	}
	
	@Test(priority=0, groups="smoke",enabled=true)
	public void verifyMultipleUserData_Tc_002(){
		String uRI=Urls.fixUrl+EndPointUrl.GET_LIST_OF_USER_PAGE_WISE.getUri("1");
		response=WebServices.Get(uRI);
		System.out.println(response.getBody().asString());
		
	}
	
	
	@Test(enabled=false)
	public void verifyGetAllUserList_TC_003(){
		String uRI= Urls.fixUrl+EndPointUrl.GET_LIST_OF_USER_PAGE_WISE.getUri("1");
		Gson gson= new GsonBuilder().create();
		UserList userList;
		/*This UserList is the pojo and Parent class which contains
		Ad and Datum class varibles and this has been converted to pojo class 
		by http://www.jsonschema2pojo.org/ site where in Source type 
		we need to select source type as Json and Annotation style we need to select as Gson
		then after converting the pojo class of Different Type we are putting in
		one package where we keep all the response in the form of pojo class*/
		response=WebServices.Get(uRI);
		userList=gson.fromJson(response.getBody().asString(), UserList.class);
		if(response.getStatusCode()==200){
			//We can also get the data by below for loop
			//int count1=0;
			//int count=1;
			for(Datum data1:userList.getData()){
			id.add(data1.getId());
			lastName.add(data1.getLastName());
			first_name.add(data1.getFirstName());
			email.add(data1.getEmail());
			url.add(data1.getAvatar());
			
				}
		}
		System.out.println(userList.getData().get(0).getFirstName());
		
			
	}
			
	@DataProvider(name = "getUserListByPageWise")
	public Object[][] getUserDetailByid() {
		Object[][] obj = new Object[id.size()][5];
		for (int i = 0; i < obj.length; i++) {
			obj[i][0] = id.get(i);
			obj[i][1] = email.get(i);
			obj[i][2] = first_name.get(i);
			obj[i][3] = lastName.get(i);
			obj[i][4] = url.get(i);
		}
		return obj;
	}

	@Test(dataProvider = "getUserListByPageWise",enabled=true)
	public void verifyUserListPageWise(Integer id1, String email1, String first_Name1, String lastName1, String url1) {
		String uRI = Urls.fixUrl + EndPointUrl.GET_LIST_OF_USER_PAGE_WISE.getUri("1");
		response = WebServices.Get(uRI);
		UserList user;
		if (response.statusCode() == 200) {
			user = gson.fromJson(response.getBody().asString(), UserList.class);
			Assert.assertEquals(id1, user.getData().get(count2).getId());
			Assert.assertEquals(email1, user.getData().get(count2).getEmail());
			Assert.assertEquals(first_Name1, user.getData().get(count2).getFirstName());
			Assert.assertEquals(lastName1, user.getData().get(count2).getLastName());
			Assert.assertEquals(url1, user.getData().get(count2).getAvatar());
			count2++;
		}

	}
			
	@Test(dataProvider = "UserDetailtoBeadded",enabled=true)
	public void verifyaddedUSerwithPostMethod(String json, String name, String designation) {
		String uRI = Urls.fixUrl + EndPointUrl.POST_USER.getUri();
		Gson gson= new GsonBuilder().create();
		UserAdd userad;
		response = WebServices.Post(uRI, json);
		System.out.println(response.asString() + "------------");
		if (response.getStatusCode() == 201) {
			userad=gson.fromJson(response.getBody().asString(), UserAdd.class);
			Assert.assertEquals(name, userad.getName());
			Assert.assertEquals(designation, userad.getJob());
		}

	}
				
	@DataProvider(name = "UserDetailtoBeadded")
	public Object[][] getUserDetailtoBeAdded() {
		Object[][] data = new Object[2][3];
		data[0][0] = "{\"name\": \"Manis\",\"job\": \"leader\"}";
		data[0][1] = "Manis";
		data[0][2] = "leader";
		data[1][0] = "{\"name\": \"Kumar\",\"job\": \"Automation\"}";
		data[1][1] = "Kumar";
		data[1][2] = "Automation";
		
		return data;
	}
	
	
	
	@Test(dataProvider="userNameandPasswordfromexcel",enabled=true)
	public void checkAuthToken(String json) throws IOException, JSONException{
	String uRI=	Urls.fixUrl+EndPointUrl.POST_LOGIN.getUri();
		String authToken=WebServices.loginToApplicationandsetAuthToken(uRI, json);
		System.out.println(authToken+"*********");
		//below assertion is to check that token is not equal to null if yes assertion will throw 
		//an error message
		System.out.println();
		Assert.assertNotNull(authToken, "token is not genearted due to some exception");
	}
	
	@DataProvider(name="userNameandPasswordfromexcel")
	//@Test
	public Object[][] getUsernameandPasswordfromExcel() throws IOException{
		wb=ConfigProp.getWorkBook();
		//String path=System.getProperty("user.dir");
		XSSFSheet sheet= (XSSFSheet) wb.getSheet("data1");
		int lastRow=sheet.getLastRowNum();
		XSSFRow row;
		System.out.println();
		Object[][] obj= new Object[lastRow][];
		for(int i=1;i<lastRow+1;i++){
			short shor=sheet.getRow(i).getLastCellNum();
			for(int j=1;j<shor+1;j++){
				obj[i-1]=new Object[j];
				obj[i-1][j-1]=sheet.getRow(i).getCell(j-1).getStringCellValue();
				System.out.println(obj[i-1][j-1]+"-------------");
				
			}
			}
		return obj;
		}

	
	//@Test(dataProvider="gettingDataforRequestParametrization")
	/*if we donot provide the dataprovider name in the dataprovider method
	then methodname itself considered as the dataprovider name and can be provided
	to the @test method*/
	@Test(dataProvider="getRequestData",enabled=true)
	public void sendRequestDataWithParameterization(String email, String pswd) throws IOException{
		javax.json.JsonObject obj=Json.createObjectBuilder().add("email", email).add("password", pswd).build();
		
		/*	This below statements comments to show how we create request with Json.createObjectBuilder()
		 * and json.createArrayBuilder();
		 
		 * Intializing one ObjectBuilder on point (1)
		 * (1)JsonObjectBuilder builder=Json.createObjectBuilder();
		 * (2)Now creating an ArrayObjectBuilder object by below line
		JsonArrayBuilder arrayBuilder=Json.createArrayBuilder();
		//adding the object in arraybuilder object on line (3) and (4)
		(3)arrayBuilder.add(Json.createObjectBuilder().add("Name","Manish").build());
		(4)arrayBuilder.add(Json.createObjectBuilder().add("Name2","Kamal").build());
		creating Jsonarray by calling build method on arrybuilder ref on line (5)
		(5)JsonArray jsonArray=arrayBuilder.build();
		now adding the array inthe main json object that is point (1) by giving it a Key
		both done in line no-(6) where EmpDetails is key to the array of object and the
		adding both in objectbuilder.
		(6)builder.add("EmpDetails",jsonArray);*/
		 
		String json=obj.toString();
		String uRI=Urls.fixUrl+EndPointUrl.POST_LOGIN.getUri();
		response=WebServices.Post(uRI,json);
		String jonString=response.getBody().asString();
		JsonParser parser= new JsonParser();
		JsonObject jsonobj=parser.parse(jonString).getAsJsonObject();
		if(response.getStatusCode()==400){
			String errorMessage=jsonobj.get("error").getAsString();
			System.out.println("Error message is :---- " +errorMessage);
			//"user not found\"
			Assert.assertEquals(errorMessage,"user not found");
		}
		else if(response.getStatusCode()==200){
		String token=jsonobj.get("token").toString();
		System.out.println(token);
		Assert.assertNotNull(token);
		}
	}
	
	
	@Test(dataProvider="getRequestData")
	public void makeRequestWithjavaUsingGson(String email, String pwd){
		Login login= new Login();
		Gson gson= new GsonBuilder().create();
		login.setEmail(email);
		login.setPassword(pwd);
		String json=gson.toJson(login);
		String uRI= Urls.fixUrl+EndPointUrl.POST_LOGIN.getUri();
		response= WebServices.Post(uRI, json);
		JsonParser parser= new JsonParser();
		JsonObject obj=parser.parse(response.getBody().asString()).getAsJsonObject();
		if(response.getStatusCode()==200){
			String token=obj.get("token").toString();
			System.out.println(token+"===============");
			Assert.assertNotNull(token);
		}
		else if(response.getStatusCode()==400){
			String errorMessage=obj.get("error").getAsString();
			System.out.println("Error message is :---- " +errorMessage);
			//"user not found\"
			Assert.assertEquals(errorMessage,"user not found");
		}
		
	}
	
	
	@DataProvider//(name="gettingDataforRequestParametrization")
	public Object[][] getRequestData() throws IOException{
		Workbook wb=ConfigProp.getWorkBook();;
		XSSFSheet sheet=(XSSFSheet)wb.getSheet("login");
		int row=sheet.getLastRowNum();
		System.out.println(row);
		Object[][] obj= new Object[row][]; 
		// initailizing row by getting the length callin method getLastRowNum(),
		/*Remember that the index here starts with 0 in case of row (eg- if rows are 3 then the count 
		will come as 2 and we are starting to get the data by row no 1 hence the condition
		for i loop would be increase by +1)*/
		Short shor=sheet.getRow(0).getLastCellNum();
		for(int i=1;i<row+1;i++){
			System.out.println(shor);
			obj[i-1]=new Object[shor];
			/*initializing our row array with "shor column length", we
			can also do it by going particular row and checking the column no
			by method getLastCellNum() and initialize the column by getting the column no;*/
			for(int j=0;j<shor;j++){
				String str=new DataFormatter().formatCellValue(sheet.getRow(i).getCell(j));
				obj[i-1][j]=str;
				if(str.isEmpty()){
					obj[i-1][j]=" ";
					System.out.println("returning empty");
							}
				if(new DataFormatter().formatCellValue(sheet.getRow(i).getCell(j))==null){
					obj[i-1][j]=" ";
					System.out.println("empty returned");
				}
				System.out.print(obj[i-1][j]+"*******");
			}
			
		}
		
		return obj;
		
	}


	
//class ends
}
				
				
	
	
